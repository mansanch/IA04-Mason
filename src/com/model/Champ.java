package com.model;

import com.outils.Constants;

import sim.engine.SimState;
import sim.engine.Stoppable;
import sim.field.grid.SparseGrid2D;
import sim.util.Int2D;

/**
 * @author Benjamin Mare - Manuel Felipe SR
 *
 */

public class Champ extends SimState {


	private	int	 numInsects;
	public int getNumInsects() { 
		return numInsects; 
	} 
	public void setNumInsects(int numInsects) { 
		this.numInsects = numInsects; 
	}
	public SparseGrid2D yard = new SparseGrid2D(Constants.getInstance().GRID_SIZE, Constants.getInstance().GRID_SIZE);
    private int num_habitants = Constants.getInstance().NUM_INSECTS;
	public Champ(long seed) {
		super(seed);
		// TODO Auto-generated constructor stub
		
	}

	@Override
	public void start() {
		// TODO Auto-generated method stub
		System.out.println("Lets go !");
		super.start();
		yard.clear();
		populateWorld();
	}

	/**
	 * Generate la position des agent aleatoirement
	 * 
	 * @return location
	 */
	private Int2D getFreeLocation() {
		Int2D location = new Int2D(random.nextInt(yard.getWidth()), random.nextInt(yard.getHeight()));
		Object ag;
		while ((ag = yard.getObjectsAtLocation(location.x, location.y)) != null) {
			location = new Int2D(random.nextInt(yard.getWidth()), random.nextInt(yard.getHeight()));
		}
		return location;
	}
	public void killOne() {
		num_habitants--;
	}

	/**
	 * Initialisation: Creation des agents dans le champ
	 */
	public void populateWorld() {
		setNumInsects(Constants.getInstance().NUM_INSECTS);
		for (int i = 0; i < Constants.getInstance().NUM_INSECTS; i++) {
			Insect a = new Insect();
			Int2D location = getFreeLocation();
			yard.setObjectLocation(a, location.x, location.y);
			a.x = location.x;
			a.y = location.y;
			//schedule.scheduleRepeating(a);
			Stoppable stop = schedule.scheduleRepeating(a);
			a.setStp(stop);
		}
		for (int i = 0; i < Constants.getInstance().NUM_FOOD_CELL; i++) {
			createFood();
		}
	}
	
	public void createFood(){
		Nourriture a = new Nourriture(this);
		Int2D location = getFreeLocation();
		yard.setObjectLocation(a, location.x, location.y);
		a.x = location.x;
		a.y = location.y;
		Stoppable stop = schedule.scheduleRepeating(a);
		a.setStp(stop);	
	}
	
	public boolean free(int x, int y)
    {
        int xx = yard.stx(x);
        int yy = yard.sty(y);
        return yard.numObjectsAtLocation(xx, yy) == 0;
    }


}

