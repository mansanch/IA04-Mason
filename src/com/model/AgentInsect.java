package com.model;

import com.outils.Constants;

import sim.engine.SimState;
import sim.engine.Steppable;
import sim.engine.Stoppable;
import sim.field.SparseField;
import sim.util.Bag;
import sim.util.Int2D;

public class AgentInsect implements Steppable {

	private int deplacement = Constants.getInstance().DISTANCE_DEPLACEMENT,
			perception = Constants.getInstance().DISTANCE_PERCEPTION,
			chargeMax = Constants.getInstance().CHARGE_MAX;
	private int energie = Constants.getInstance().MAX_ENERGY;
	private int charge = 0;
	private boolean alive = true;
	public int x, y;
	private Champ champ;
	private Stoppable stp;
	private Bag stock;

	public AgentInsect() {
		// r�partition points de capacit�
		int capacite = Constants.getInstance().CAPACITY;
		int repartition = (int) (Math.random() * capacite);
		perception += repartition;
		capacite -= repartition;
		repartition = (int) (Math.random() * capacite);
		deplacement += repartition;
		capacite -= repartition;
		chargeMax += capacite;
	}

	public void step(SimState arg0) {
		champ = (Champ) arg0;
		if (energie == 0)
			kill();
		else if (alive) {
			Bag adjacent = champ.yard.getMooreNeighbors(x, y, 1, SparseField.ANY_SIZE, false);
			if (charge == chargeMax
					&& energie < Constants.getInstance().MAX_ENERGY) {
				// if charge is full and energy isn't
				System.out.println("eat");
				eat(charge);
			} else if (perceiveFood(adjacent) && charge < chargeMax) {
				// if there is food in local cell get some
				grabFood(adjacent,chargeMax - charge);
				System.out.println("getFood");
			} else if (energie == 1 && charge > 0) {
				// if insect need to eat to survive
				System.out.println("eat");
				eat(charge);
			} else {
				// else, go find some food
				moveToFood();
			}
		}

	}

	// TODO
	private void moveToFood() {
		// update neighborhood
		perception();
		boolean food = false;
		int i = 0;
		while (i < stock.size() && !food) {
			Object o = stock.get(i);
			if (o instanceof Nourriture) {
				food = true;
				Nourriture foodEntity = (Nourriture) o;
				int xToGo = x;
                int yToGo = y;
                if (xToGo > foodEntity.x)
                {
                    xToGo--;
                }
                else if (xToGo < foodEntity.x)
                {
                    xToGo++;
                }
                if (yToGo > foodEntity.y)
                {
                    yToGo--;
                }
                else if (yToGo < foodEntity.y)
                {
                    yToGo++;
                }
                if (xToGo != x || yToGo != y)
                {
                	
                    moveToDistance(xToGo, yToGo);
                }
                System.out.println("toFood");
			}
			i++;
		}
		if(!food){
			System.out.println("random move");
			move();
			addEnergy(-1);
		}
	}

	public boolean perceiveFood(Bag adjacent)
    {
        boolean food = false;
        int i = 0;
        while (i < adjacent.size() && !food)
        {
            Object o = adjacent.get(i);
            if (o instanceof Nourriture)
            {
                food = true;
            }
            i++;
        }
        return food;
    }


	/**
	 * only call with negative integers, if using a positive one please use the
	 * "eat()" function
	 * 
	 * @param modification
	 *            integer
	 */
	private void addEnergy(int modification) {
		energie += modification;
		if (energie <= 0) {
			kill();
		}
	}

	public void eat(int howMuch) {
		int ate = Math.min(charge, howMuch);
		charge = Math.max(charge - howMuch, 0);
		addEnergy(ate);
	}

	public int getFreeSpace() {
		return charge - chargeMax;
	}

	public Stoppable getStp() {
		return stp;
	}

	public void setStp(Stoppable stp) {
		this.stp = stp;
	}

	public void addCharge(int nbCharges) {
		if (!(nbCharges + charge > chargeMax)) {
			charge += nbCharges;
		}
	}

	private void kill() {
		champ.setNumInsects(champ.getNumInsects()-1);
		alive = false;
		champ.yard.remove(this);
		champ.killOne();
		getStp().stop();
		champ = null;
		stp = null;
	}

	private void perception() {
		stock = champ.yard.getMooreNeighbors(this.x, this.y, this.perception,
				SparseField.ANY_SIZE, false);
	}

	/**
	 * Check if the movement to the specific destination is possible
	 * 
	 * @param destinationX
	 * @param destinationY
	 * @param Champ
	 * @return boolean
	 */
	public void moveToDistance(int destinationX, int destinationY) {
		int distance = Math.max(Math.abs(x - destinationX),
				Math.abs(y - destinationY));
		// d'abord se d�placer, permet ainsi de v�rifier si le d�placement
		// provoque la mort de l'insecte
		if (distance > deplacement){
			destinationX = (Math.abs(destinationX)>deplacement)?deplacement:destinationX;
			destinationY = (Math.abs(destinationY)>deplacement)?deplacement:destinationY;
		}
		addEnergy(-1);
		if (alive) {
			System.out.println("moving from" + x + " " + y + "to"
					+ destinationX + " " + destinationY);
			champ.yard.removeObjectsAtLocation(this);
			x = champ.yard.stx(destinationX);
			y = champ.yard.sty(destinationY);
			champ.yard.setObjectLocation(this, x, y);
		}
	}
	
	public void grabFood(Bag adjacent,int howMuch)
    {
		int possibleHowMuch = Math.min(howMuch, chargeMax - charge);
        boolean grab = false;
        int i = 0;
        while (i < adjacent.size() && !grab)
        {
            Object o = adjacent.get(i);
            if (o instanceof Nourriture)
            {
            	Nourriture nour = (Nourriture) o;
            	charge += nour.getFreeFood(possibleHowMuch);
                grab = true;
            }
            i++;
        }
    }


	public boolean move() {
		boolean done = false;

		int n = champ.random.nextInt(Constants.getInstance().NB_DIRECTIONS);
		switch (n) {
		case 0:
			if (champ.free(x - 1, y)) {
				champ.yard.removeObjectsAtLocation(this);
				champ.yard.setObjectLocation(this, x - 1, y);
				x = champ.yard.stx(x - 1);
				done = true;
			}
			break;
		case 1:
			if (champ.free(x + 1, y)) {
				champ.yard.removeObjectsAtLocation(this);
				champ.yard.setObjectLocation(this, x + 1, y);
				x = champ.yard.stx(x + 1);
				done = true;
			}
			break;
		case 2:
			if (champ.free(x, y - 1)) {
				champ.yard.removeObjectsAtLocation(this);
				champ.yard.setObjectLocation(this, x, y - 1);
				y = champ.yard.sty(y - 1);
				done = true;
			}
			break;
		case 3:
			if (champ.free(x, y + 1)) {
				champ.yard.removeObjectsAtLocation(this);
				champ.yard.setObjectLocation(this, x, y + 1);
				y = champ.yard.sty(y + 1);
				done = true;
			}
			break;
		case 4:
			if (champ.free(x - 1, y - 1)) {
				champ.yard.removeObjectsAtLocation(this);
				champ.yard.setObjectLocation(this, x - 1, y - 1);
				x = champ.yard.stx(x - 1);
				y = champ.yard.sty(y - 1);
				done = true;
			}
			break;
		case 5:
			if (champ.free(x + 1, y - 1)) {
				champ.yard.removeObjectsAtLocation(this);
				champ.yard.setObjectLocation(this, x + 1, y - 1);
				x = champ.yard.stx(x + 1);
				y = champ.yard.sty(y - 1);
				done = true;
			}
			break;
		case 6:
			if (champ.free(x + 1, y + 1)) {
				champ.yard.removeObjectsAtLocation(this);
				champ.yard.setObjectLocation(this, x + 1, y + 1);
				x = champ.yard.stx(x + 1);
				y = champ.yard.sty(y + 1);
				done = true;
			}
			break;
		case 7:
			if (champ.free(x - 1, y + 1)) {
				champ.yard.removeObjectsAtLocation(this);
				champ.yard.setObjectLocation(this, x - 1, y + 1);
				x = champ.yard.stx(x - 1);
				y = champ.yard.sty(y + 1);
				done = true;
			}
			break;
		}
		return done;
	}

}
