package com.main;

import com.gui.BeingsWithUI;
import com.model.Beings;
import com.model.Champ;

import sim.display.Console;


public class BeingsMain {
	public static void main(String[] args) {
        runUI();
	}
	public static void runUI() {
		Champ model = new Champ(System.currentTimeMillis());
		BeingsWithUI gui = new BeingsWithUI(model);
		Console console = new Console(gui);
		console.setVisible(true);
	}
	
}
