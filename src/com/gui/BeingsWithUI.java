package com.gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.TexturePaint;
import java.awt.image.BufferedImage;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JFrame;

import com.model.Beings;
import com.model.Champ;
import com.model.Insect;
import com.model.Nourriture;
import com.model.TypeA;
import com.model.TypeB;

import sim.display.Controller;
import sim.display.Display2D;
import sim.display.GUIState;
import sim.engine.SimState;

import sim.portrayal.Inspector;

import sim.portrayal.grid.SparseGridPortrayal2D;
import sim.portrayal.simple.OvalPortrayal2D;


public class BeingsWithUI extends GUIState {
	public static int FRAME_SIZE = 600;
	public Display2D display;
	public JFrame displayFrame;
	SparseGridPortrayal2D yardPortrayal = new SparseGridPortrayal2D();
	
	public BeingsWithUI(SimState state) {
		super(state);
	}
	public SparseGridPortrayal2D getYardPortrayal() {
		return yardPortrayal;
	}
	public void setYardPortrayal(SparseGridPortrayal2D yardPortrayal) {
		this.yardPortrayal = yardPortrayal;
	}
	public static String getName() {
		return "Simulation de cr�atures"; 
	}
	public void start() {
	  super.start();
	  setupPortrayals();
	}

	public void load(SimState state) {
	  super.load(state);
	  setupPortrayals();
	}
	
	private OvalPortrayal2D getInsectPortrayal() {
		StrangeOvalPortrayal r = new StrangeOvalPortrayal(new Color(110,174,0));
	    
		r.filled = true;
		return r;
	}
	private OvalPortrayal2D getNourriturePortrayal() {
		OvalPortrayal2D r = new OvalPortrayal2D();
		r.paint = new Color(255,42,54);
		
		r.filled = true;
		
		return r;
	}
	public void setupPortrayals() {
		  Champ champ = (Champ) state;	
		  yardPortrayal.setField(champ.yard);
		  yardPortrayal.setPortrayalForClass(Insect.class,getInsectPortrayal());
		  yardPortrayal.setPortrayalForClass(Nourriture.class,getNourriturePortrayal());
		  //yardPortrayal.setPortrayalForClass(TypeB.class, new StrangeOvalPortrayal());
		   display.reset();
		
		  display.setBackdrop(new Color(109,72,16));
		  display.repaint();
			// redraw the display
		//  addBackgroundImage();
	
	 
	}

	public void init(Controller c) {
		  super.init(c);
		  display = new Display2D(FRAME_SIZE,FRAME_SIZE,this);
		  display.setClipping(false);
		  displayFrame = display.createFrame();
		  displayFrame.setTitle("Champ");
		  c.registerFrame(displayFrame); // so the frame appears in the "Display" list
		  displayFrame.setVisible(true);
		  display.attach( yardPortrayal, "Yard" );
		}
	private void addBackgroundImage() {
	  Image i = new ImageIcon(getClass().getResource("back.jpg")).getImage();
	  int w = i.getWidth(null)/5;
	  int h = i.getHeight(null)/5;
	  BufferedImage b = display.getGraphicsConfiguration().createCompatibleImage(w,h);
	  Graphics g = b.getGraphics();
	  g.drawImage(i,0,0,w,h,null);
	  g.dispose();
	  display.setBackdrop(new TexturePaint(b, new Rectangle(0,0,w,h)));
	}
	public  Object  getSimulationInspectedObject()  {  return  state;  }
	public  Inspector  getInspector() {
	Inspector  i  =  super.getInspector();
	  i.setVolatile(true);
	  return  i;
	}
}
