package com.gui;
import java.awt.Color;
import java.awt.Graphics2D;

import com.model.AgentInsect;
import com.model.AgentType;
import com.model.Insect;

import sim.portrayal.DrawInfo2D;
import sim.portrayal.simple.OvalPortrayal2D;


public class StrangeOvalPortrayal extends OvalPortrayal2D {
	Insect agent ;
	public StrangeOvalPortrayal(Color color) {
		super(color);
		this.paint = color;
		
	}

	
	@Override
	public void draw(Object object, Graphics2D graphics, DrawInfo2D info) {
		//AgentInsect agent = (AgentInsect)object;
		 agent = (Insect)object;
	     
		if (agent.x % 5 == 0 && agent.y % 5 == 0)
           scale = 2;
		 else scale = 1;
		
			
			super.draw(object, graphics, info);
			
			
	}
	

}

