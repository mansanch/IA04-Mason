package com.outils;

public class Constants {
	public final int GRID_SIZE = 30; 
	public final int NUM_INSECTS = 10;
	public final int NUM_FOOD_CELL = 15;
	public final int MAX_ENERGY = 15;
	public final int MAX_LOAD = 5;
	public final int CAPACITY = 10;
	public final int MAX_FOOD = 5;
	public final int FOOD_ENERGY = 3;
	public final int NUM_A = 100; 
	public final int NUM_B = 100; 
	public final int DISTANCE_DEPLACEMENT =1;
	public final int DISTANCE_PERCEPTION =1;
	public final int CHARGE_MAX =1;
	public final int NB_DIRECTIONS = 8;
	/** Constructeur priv� */
    private Constants() 
    {}
     
    /** Instance unique non pr�initialis�e */
    private static Constants INSTANCE = null;
     
    /** Point d'acc�s pour l'instance unique du singleton */
    public static Constants getInstance()
    {           
        if (INSTANCE == null)
        {   INSTANCE = new Constants(); 
        }
        return INSTANCE;
    }
}
